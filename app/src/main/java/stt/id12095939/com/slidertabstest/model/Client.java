/*
 * Copyright (C) 2016 Mykyta Shulhin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package stt.id12095939.com.slidertabstest.model;


public class Client {

    private int mClientNumber;
    private String mPassword;
    private String mFullName;
    private int mSavingGoal;


    public Client(int clientNumber, String password, String fullName, int savingGoal){
        this.mClientNumber = clientNumber;
        this.mPassword = password;
        this.mFullName = fullName;
        this.mSavingGoal = savingGoal;

    }

    public int getClientNumber() {
        return mClientNumber;
    }

    public String getPassword() {
        return mPassword;
    }

    public String getFullName() {
        return mFullName;
    }

    public int getSavingGoal() {
        return mSavingGoal;
    }
}
