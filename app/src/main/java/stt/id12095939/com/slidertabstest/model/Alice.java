/*
 * Copyright (C) 2016 Mykyta Shulhin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package stt.id12095939.com.slidertabstest.model;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Alice {
    public MyDatabaseHelper mDb;
    public Context mContext;
    public Brain mBrain;
    public int mSavingGoal;
    public Transaction mMostExpensive;
    public boolean mRemind_flag = false;
    public boolean mSave_flag = false;
    public String mMenu = "\n -'Advice' \n -'Remind me' \n -'My payments' \n -'Save' \n - 'Inspire me' \n " + "If you want to see this instruction again, simply send me: 'help'";

    public Alice(Context context) throws ParseException {
        mBrain = new Brain();
        this.mContext = context;
        initialize();
    }

    /**
     * This method initializes class attributes.
     */
    public void initialize() throws ParseException {
        mDb = new MyDatabaseHelper(mContext);
        mSavingGoal = Integer.parseInt(mDb.getSavingGoal());
        mBrain.analyzeTransactions(mDb.getTransactionObjects("all"));
        mMostExpensive = mBrain.getMostExpensive();
    }

    /**
     * This method is launched when AssistantActivity is created. It send greetings message to user.
     */
    public String greetings() {
        return "Hi! My name is Alice and I'm your personal advisor. Feel free to ask me questions :) \n You can ask me: " + mMenu;
    }

    /**
     * This method is gets a user's query, analyzes it and produces an answer output.
     */
    public String ask(String question) throws ParseException {
        String answer = "";
        question = question.toUpperCase();
        if (mRemind_flag) {
            if (mBrain.findTransaction(question) != null) {
                addCalendarEvent(mBrain.findTransaction(question));
                answer = "Opening calendar for you...";
                mRemind_flag = false;
            } else {
                answer = "Sorry, couldn't find that payment... Are you sure you spelled it right?";
                mRemind_flag = false;
            }
        } else {
            if (mSave_flag) {
                if (mBrain.findTransaction(question) != null) {
                    mDb.updateTransactionStatus("true", String.valueOf(mBrain.findTransaction(question).getID()));
                    answer = "Great, now " + question + " payment is marked as 'saved'.";
                    mBrain.analyzeTransactions(mDb.getTransactionObjects("all"));
                    mSave_flag = false;
                } else {
                    answer = "Sorry, couldn't find that payment... Are you sure you spelled it right?";
                    mSave_flag = false;
                }
            } else {
                if (question.length()>13) {
                    Transaction found = mBrain.findTransactionByID(question.substring(15, question.length()));
                    if (found!= null&&question.substring(0, 13).equals("ADVICE FOR ID")) {
                        String base = "Thank you for waiting! You requested advice about " + found.getReceiver() + " payment. It is equal to "+ calculatePersantageFromGoal(found.getAmount()) + "% of your saving goal, ";
                        if (found.getAmount() < mMostExpensive.getAmount()) {
                            answer = base + "which is less than the most expensive transaction - "+mMostExpensive.getReceiver()+". If" +
                                    " this payment is essential, you may not mark it as saved. I suggest to have a look on "+mMostExpensive.getReceiver()+"" +
                                    " which is $"+mMostExpensive.getAmount()+" per month and save it if possible.";
                        } else {
                            answer = base + " I definately suggest you to have a closer look on this payment. My analysis showed that "+found.getReceiver()+" is the most expensive " +
                                    "transaction in the list. Try to evaluate whether it is important or not, because it can help you to achieve your goal faster.";
                        }
                    } else {
                        answer = "For some reason I can't assist you with this payment. Please, try to refresh your transaction list" +
                                " and try again.";
                    }
                } else {
                    switch (question) {
                        case "HI":
                            answer = "Hi, how are you?";
                            break;
                        case "HELLO":
                            answer = "Hello, how are you?";
                            break;
                        case "HEY":
                            answer = "Hey, how are you?";
                            break;
                        case "THANKS":
                            answer = "No worries :) Always glad to help you";
                            break;
                        case "THANK YOU":
                            answer = "No worries :) Always glad to help you";
                            break;
                        case "GOOD":
                            answer = "Nice to hear that";
                            break;
                        case "NOT BAD":
                            answer = "Nice to hear that";
                            break;
                        case "FINE":
                            answer = "Nice to hear that";
                            break;
                        case "INSPIRE ME":
                            answer = Values.quotes[RandomGen.getRandom(Values.quotes.length, 0)];
                            break;
                        case "MY PAYMENTS":
                            answer = "Here is a list of your payments and their status:";
                            for (String current : mBrain.getPaymentNames()) {
                                answer += "\n -" + current;
                            }
                            answer += "\n You can save or remind these transactions by asking me. If you forgot how to, send me 'help'";
                            break;
                        case "SAVE":
                            answer = "Which payment do you want to mark as saved?";
                            mSave_flag = true;
                            break;
                        case "REMIND ME":
                            answer = "What payment do you want me to remind about?";
                            mRemind_flag = true;
                            break;
                        case "HELP":
                            answer = "You can ask me: " + mMenu;
                            break;
                        case "ADVICE":
                            if (mMostExpensive == null) {
                                answer = "Let's look at your profile. Your saving goal is $" + mSavingGoal + " per month, right? " +
                                        "For some reason, I couldn't find the most expensive transaction to save on. Probably you don't have enough data for my analysys or all your payments are marked as saved. If so - good job!";
                            } else {
                                Log.d("ALICE", "MOST EXPENSIVE: " + mMostExpensive + ", SAVING GOAL: " + mSavingGoal);
                                answer = "Let's look at your profile. Your saving goal is $" + mSavingGoal + ", right? My advice is to have a closer look on " + mMostExpensive.getReceiver() + " payment, which is not marked as 'saved'." +
                                        " You pay $" + mMostExpensive.getAmount() + " per month, which is %" + calculatePersantageFromGoal(mMostExpensive.getAmount()) + " from your goal.";

                            }
                            break;
                        default:
                            answer = "Sorry, I don't know what to reply... Try to use 'help'";
                            break;
                    }
                }
            }
        }
        return answer;
    }

    /**
     * This method calculates percentage from saving goal and formats it to #0.00 point.
     */
    public String calculatePersantageFromGoal(Double value) {
        DecimalFormat df = new DecimalFormat("#0.00");
        return df.format(value * 100 / mSavingGoal);
    }

    /**
     * This method adds and event to the calendar. It takes Transaction object and extracts information for event.
     */
    public void addCalendarEvent(Transaction transaction) throws ParseException {
        Calendar cal = Calendar.getInstance();
        Log.d("DATE ", "DATE IS : " + transaction.getDate());
        cal.setTime(new SimpleDateFormat("MM/dd/yyyy").parse(transaction.getDate()));
        Intent intent = new Intent(Intent.ACTION_EDIT);
        intent.setType("vnd.android.cursor.item/event");
        intent.putExtra("beginTime", cal.getTimeInMillis());
        intent.putExtra("allDay", false);
        intent.putExtra("rrule", "FREQ=YEARLY");
        intent.putExtra("endTime", cal.getTimeInMillis() + 60 * 60 * 1000);
        intent.putExtra("title", transaction.getReceiver() + " payment: $" + transaction.getAmount());
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }

}
