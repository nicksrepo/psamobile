/*
 * Copyright (C) 2016 Mykyta Shulhin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package stt.id12095939.com.slidertabstest.controller;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;

import android.os.AsyncTask;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import stt.id12095939.com.slidertabstest.model.Client;
import stt.id12095939.com.slidertabstest.model.MyDatabaseHelper;
import stt.id12095939.com.slidertabstest.R;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    private ProgressBar mProgressBar;
    private Button mSignIn;
    private EditText mClientNumber;
    private EditText mPassword;
    private ImageView mSuccess;
    private ImageView mLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getWindow().setNavigationBarColor(Color.parseColor("#0097A7"));
        initialize();
        mSignIn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String clientNumber = mClientNumber.getText().toString();
                String password = mPassword.getText().toString();
                if (clientNumber.isEmpty()) {
                    mClientNumber.setError("Please, input your client number");
                    return;

                } else {
                    if (password.isEmpty()) {
                        mPassword.setError("Please, input your password");
                        return;
                    } else {
                        LoginAsync login = new LoginAsync();
                        login.execute(clientNumber, password);
                    }
                }


            }
        });

    }

    private void initialize() {
        mSignIn = (Button) findViewById(R.id.sign_in_button);
        mClientNumber = (EditText) findViewById(R.id.client_number);
        mPassword = (EditText) findViewById(R.id.password);
        mProgressBar = (ProgressBar) findViewById(R.id.login_progress);
        mSuccess = (ImageView) findViewById(R.id.success_image);
        mLogo = (ImageView) findViewById(R.id.logo_login);
    }


    public class LoginAsync extends AsyncTask<String, Void, Integer> {

        MyDatabaseHelper mDb;
        Client mClient;

        /**
         * Actiones that must be performed before the execution
         */

        @Override
        protected void onPreExecute() {
            mLogo.setVisibility(View.GONE);
            mProgressBar.setVisibility(View.VISIBLE);
        }


        /**
         * Actiones that must be performed in a background
         *
         * @param
         * @return
         */

        @Override
        protected Integer doInBackground(String... values) {
            int result = 0;
            try {
                Thread.sleep(2000);
                mDb = new MyDatabaseHelper(getApplicationContext());
                int clientNumber = mDb.getClientObject().getClientNumber();
                String password = mDb.getClientObject().getPassword();
                if (Integer.parseInt(values[0]) == clientNumber && values[1].equals(password)) {
                    result = 1;
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return result;
        }

        /**
         * Actions that must be performed after execution
         *
         * @param integer
         */

        @Override
        protected void onPostExecute(Integer integer) {

            switch (integer) {
                case (0):
                    mProgressBar.setVisibility(View.GONE);
                    mLogo.setVisibility(View.VISIBLE);
                    Toast.makeText(getApplicationContext(), "Wrong username or password, try again.",
                            Toast.LENGTH_LONG).show();
                    break;
                case (1):
                    mProgressBar.setVisibility(View.GONE);
                    mSuccess.setVisibility(View.VISIBLE);
                    Intent logged = new Intent(getApplicationContext(), MainActivity.class);
                    logged.putExtra("logged", "true");
                    startActivity(logged);
                    finish();
                    break;
            }
        }

        /**
         * Actiones performed after the progress received
         *
         * @param values
         */

        @Override
        protected void onProgressUpdate(Void... values) {


        }
    }

}