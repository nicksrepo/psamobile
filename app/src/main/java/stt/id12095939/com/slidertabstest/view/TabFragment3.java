/*
 * Copyright (C) 2016 Mykyta Shulhin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package stt.id12095939.com.slidertabstest.view;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Switch;

import com.skyfishjy.library.RippleBackground;


import java.text.ParseException;

import stt.id12095939.com.slidertabstest.model.Client;
import stt.id12095939.com.slidertabstest.model.MyDatabaseHelper;
import stt.id12095939.com.slidertabstest.R;
import stt.id12095939.com.slidertabstest.model.Values;

public class TabFragment3 extends Fragment {
    private View mTab;
    private EditText mSavingInput;
    private MyDatabaseHelper mDb;
    private Button mSetUpGoal;
    private ProgressBar mProgressBar;
    private Switch mSmartWatchSwitch;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mTab = inflater.inflate(R.layout.tab_fragment_3, container, false);
        initialize();
        mSetUpGoal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SettingsAsync save = new SettingsAsync();
                String savingGoal = mSavingInput.getText().toString();
                if (savingGoal.isEmpty()) {
                    mSavingInput.setError(Values.GOAL_INPUT_ERROR);
                    return;
                } else {
                    save.execute(savingGoal);
                }

            }
        });
        mSmartWatchSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    final Dialog dialog = new Dialog(mTab.getContext());
                    dialog.setContentView(R.layout.watch_connect_window);
                    WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
                    dialog.show();
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
                    lp.dimAmount = 0.7f;
                    dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
                    dialog.getWindow().setAttributes(lp);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            mSmartWatchSwitch.setChecked(false);
                        }
                    });
                    final RippleBackground rippleBackground=(RippleBackground)dialog.findViewById(R.id.content);
                    ImageView imageView=(ImageView)dialog.findViewById(R.id.centerWatchImage);
                    rippleBackground.startRippleAnimation();
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                        }
                    });
                }else{

                }
            }
        });
        return mTab;
    }



    public void setSavingGoal() {
        TabFragment2.sSavingGoal.setText(Values.DOLLAR+mDb.getSavingGoal());
    }

    public void setUpChartValues(){
        try {
            TabFragment2.setUpChartTest();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void initialize() {
        mDb = new MyDatabaseHelper(getContext());
        mSavingInput = (EditText) mTab.findViewById(R.id.savin_goal_input);
        mSetUpGoal = (Button) mTab.findViewById(R.id.setup_goal_button);
        mProgressBar = (ProgressBar) mTab.findViewById(R.id.settings_progress);
        mSmartWatchSwitch = (Switch) mTab.findViewById(R.id.smartwatch_switch);
    }


    public class SettingsAsync extends AsyncTask<String, Void, Integer> {

        Client mClient;

        /**
         * Actiones that must be performed before the execution
         */

        @Override
        protected void onPreExecute() {
            mProgressBar.setVisibility(View.VISIBLE);
        }


        /**
         * Actiones that must be performed in a background
         *
         * @param
         * @return
         */

        @Override
        protected Integer doInBackground(String... values) {
            int result = 0;
            try {
                Thread.sleep(2000);
                mDb.updateSavingGoal(values[0]);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return result;
        }

        /**
         * Actions that must be performed after execution
         *
         * @param integer
         */

        @Override
        protected void onPostExecute(Integer integer) {
            setSavingGoal();
            setUpChartValues();
            mProgressBar.setVisibility(View.GONE);
            Snackbar snackbar = Snackbar
                    .make(mTab, Values.SUCCESS, Snackbar.LENGTH_LONG);
            snackbar.show();
        }

        /**
         * Actiones performed after the progress received
         *
         * @param values
         */

        @Override
        protected void onProgressUpdate(Void... values) {


        }
    }


}