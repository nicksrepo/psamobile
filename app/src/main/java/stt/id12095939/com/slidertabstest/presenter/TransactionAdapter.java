/*
 * Copyright (C) 2016 Mykyta Shulhin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package stt.id12095939.com.slidertabstest.presenter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;

import stt.id12095939.com.slidertabstest.model.MyDatabaseHelper;
import stt.id12095939.com.slidertabstest.R;
import stt.id12095939.com.slidertabstest.model.Transaction;
import stt.id12095939.com.slidertabstest.model.Values;
import stt.id12095939.com.slidertabstest.controller.SaveActivity;


public class TransactionAdapter extends RecyclerView.Adapter<TransactionAdapter.MyViewHolder> {
    private View mView;
    private LinkedList<Transaction> transactionsList;
    private MyDatabaseHelper mDb;
    public Context mContext;


    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView receiver_img;
        public TextView receiver_txt;
        public TextView amount;
        public Button saveButton;
        public ImageView calendarButton;

        public MyViewHolder(View view) {
            super(view);
            receiver_img = (ImageView) view.findViewById(R.id.receiver_item_img);
            receiver_txt = (TextView) view.findViewById(R.id.receiver_item_txt);
            amount = (TextView) view.findViewById(R.id.amount_item);
            saveButton = (Button) view.findViewById(R.id.but_save_item);
            calendarButton = (ImageView) view.findViewById(R.id.remind_calendar_button);

        }
    }

    public TransactionAdapter(LinkedList<Transaction> transactionsList) {
        this.transactionsList = transactionsList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        mView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view, parent, false);
        mDb = new MyDatabaseHelper(mView.getContext());
        return new MyViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Transaction transaction = transactionsList.get(position);
        final String amount = String.valueOf(Values.MINUS + Values.DOLLAR + transaction.getAmount());
        holder.amount.setText(amount);

        int drawable;
        String receiver = transaction.getReceiver();
        //temporary logic, can do better later
        if (receiver.equalsIgnoreCase("Netflix")) {
          drawable = R.drawable.netflix_logo;
        } else if (receiver.equalsIgnoreCase("Hulu")) {
          drawable = R.drawable.hulu_logo;
        } else if (receiver.equalsIgnoreCase("discovery")) {
          drawable = R.drawable.discovery_logo;
        } else if (receiver.equalsIgnoreCase("Optus")) {
          drawable = R.drawable.optus_logo;
        } else  {
          drawable = 0;
        }

        if (drawable != 0) {
          Picasso.with(mContext).load(drawable).into(holder.receiver_img);
          holder.receiver_img.setVisibility(View.VISIBLE);
        } else {
          holder.receiver_txt.setText(receiver);
          holder.receiver_txt.setVisibility(View.VISIBLE);
        }

        if (transaction.isImportant()) {
            holder.amount.setTextColor(Color.RED);
        }else{
            holder.amount.setTextColor(Color.parseColor("#727272"));
        }


        if (mDb.getSavingGoal().equals("0")) {
            holder.saveButton.setEnabled(false);
            holder.saveButton.setTextColor(Color.RED);
            holder.saveButton.setText(Values.SETUP_GOAL);
        }

        holder.calendarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    addCalendarEvent(transaction);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });

        holder.saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent saveLauncher = new Intent(mView.getContext(), SaveActivity.class);
                saveLauncher.putExtra("amount", transaction.getAmount());
                saveLauncher.putExtra("date", transaction.getDate());
                saveLauncher.putExtra("ID", transaction.getID());
                saveLauncher.putExtra("receiver", transaction.getReceiver());
                saveLauncher.putExtra("status", String.valueOf(transaction.getImportantStatus()));
                saveLauncher.putExtra("description", transaction.getDescription());
                mView.getContext().startActivity(saveLauncher);
            }
        });

        setUpSwipeLayout(holder);

    }

    public void setUpSwipeLayout(MyViewHolder holder) {
        SwipeLayout swipeLayout = (SwipeLayout) mView.findViewById(R.id.swipe_layout);

        swipeLayout.setShowMode(SwipeLayout.ShowMode.LayDown);

        swipeLayout.addDrag(SwipeLayout.DragEdge.Left, mView.findViewById(R.id.bottom_wrapper));

        swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
            @Override
            public void onClose(SwipeLayout layout) {
                //when the SurfaceView totally cover the BottomView.
            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {
                //you are swiping.
            }

            @Override
            public void onStartOpen(SwipeLayout layout) {

            }

            @Override
            public void onOpen(SwipeLayout layout) {
                //when the BottomView totally show.
            }

            @Override
            public void onStartClose(SwipeLayout layout) {

            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {
                //when user's hand released.
            }
        });
    }

    public void addCalendarEvent(Transaction transaction) throws ParseException {
        Calendar cal = Calendar.getInstance();
        Log.d("DATE", "DATE IS : " + transaction.getDate());
        cal.setTime(new SimpleDateFormat("MM/dd/yyyy").parse(transaction.getDate()));
        Intent intent = new Intent(Intent.ACTION_EDIT);
        intent.setType("vnd.android.cursor.item/event");
        intent.putExtra("beginTime", cal.getTimeInMillis());
        intent.putExtra("allDay", false);
        intent.putExtra("rrule", "FREQ=YEARLY");
        intent.putExtra("endTime", cal.getTimeInMillis() + 60 * 60 * 1000);
        intent.putExtra("title", transaction.getReceiver() + " payment: $" + transaction.getAmount());
        mView.getContext().startActivity(intent);
    }


    @Override
    public int getItemCount() {
        return transactionsList.size();
    }

    public void updateTransactionsList(LinkedList<Transaction> newList) {
        transactionsList.clear();
        transactionsList.addAll(newList);
        this.notifyDataSetChanged();

    }


}
