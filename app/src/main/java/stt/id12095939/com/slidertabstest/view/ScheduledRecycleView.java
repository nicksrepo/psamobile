/*
 * Copyright (C) 2016 Mykyta Shulhin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package stt.id12095939.com.slidertabstest.view;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.yalantis.phoenix.PullToRefreshView;

import java.text.ParseException;
import java.util.LinkedList;

import stt.id12095939.com.slidertabstest.model.Brain;
import stt.id12095939.com.slidertabstest.model.MyDatabaseHelper;
import stt.id12095939.com.slidertabstest.model.Notification;
import stt.id12095939.com.slidertabstest.R;
import stt.id12095939.com.slidertabstest.model.Transaction;
import stt.id12095939.com.slidertabstest.presenter.TransactionAdapter;


public class ScheduledRecycleView extends Fragment {

    private TransactionAdapter mTransactionAdapter;
    private RecyclerView mRecyclerView;
    private LinkedList<Transaction> mTransactionsList = new LinkedList<Transaction>();
    private View mResult;
    private MyDatabaseHelper mDb;
    private PullToRefreshView mSwipeRefreshLayout;
    private Brain mBrain;
    private ProgressBar mProgressBar;


    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        if (mResult == null) {
            mBrain = new Brain();
            mResult = inflater.inflate(R.layout.scheduled_recycler_view_fragment, container, false);
            mRecyclerView = (RecyclerView) mResult.findViewById(R.id.recycler_view);
            mProgressBar = (ProgressBar) mResult.findViewById(R.id.loading_progress);
            final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            mRecyclerView.setLayoutManager(layoutManager);
            mDb = new MyDatabaseHelper(getContext());
            mSwipeRefreshLayout = (PullToRefreshView) mResult.findViewById(R.id.pull_to_refresh);
            LoadingAsync startUp = new LoadingAsync();
            startUp.execute();
            mTransactionAdapter = new TransactionAdapter(mTransactionsList);
            mRecyclerView.setAdapter(mTransactionAdapter);
            mSwipeRefreshLayout.setOnRefreshListener(new PullToRefreshView.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    try {
                        mBrain = new Brain();
                        //mDb.deleteAllFromTable(mDb.getTransactionsTableName());
                        //mDb.populateDb(5, 10);
                        setUpTransactions();
                        Notification notification = new Notification(getContext(), "PSA", "Transactions updated!");
                        mTransactionAdapter.updateTransactionsList(mBrain.analyzeTransactions(mTransactionsList));
                        Toast.makeText(getContext(), "Transactions have been updated.",
                                Toast.LENGTH_LONG).show();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                    }, 0);
                }
            });

        }
        return (mResult);
    }


    public void deleteTransactionsListAndSql() {
        mDb.deleteAllFromTable(mDb.getTransactionsTableName());
        mTransactionsList.clear();
    }

    public void showListContent() {
        for (int i = 0; i < mTransactionsList.size(); i++) {
            Transaction current = mTransactionsList.get(i);
            System.out.println("ID:" + current.getID() + ", DATE:" + current.getDate() + ", DESC:" + current.getDescription() + ", RECIEV:" + current.getReceiver() + ", AMOUNT:" + current.getAmount());
        }
    }

    public void setUpTransactions() throws ParseException {
        mTransactionsList = mDb.getTransactionObjects("all");
        showListContent();
    }

    public LinkedList<Transaction> getFakeTransactions(int transactions) {
        LinkedList<Transaction> data = new LinkedList<Transaction>();
        for (int i = 0; i < transactions; i++) {
            data.add(new Transaction("TEST" + i, 10, "10/04/2016", "DESCR", i, true));
        }
        return data;
    }


    public class LoadingAsync extends AsyncTask<Void, Void, Integer> {

        /**
         * Actions that must be performed before the execution
         */

        @Override
        protected void onPreExecute() {
            mProgressBar.setVisibility(View.VISIBLE);
        }


        /**
         * Actions that must be performed in a background
         */

        @Override
        protected Integer doInBackground(Void... values) {
            int result = 0;
            try {
//                mDb.deleteAllFromTable(mDb.getTransactionsTableName());
//                mDb.populateDb(5, 10);
                setUpTransactions();
                mTransactionsList = mBrain.analyzeTransactions(mTransactionsList);


            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }

        /**
         * Actions that must be performed after execution
         *
         * @param integer
         */

        @Override
        protected void onPostExecute(Integer integer) {
            mTransactionAdapter.updateTransactionsList(mTransactionsList);
            mProgressBar.setVisibility(View.GONE);
        }

        /**
         * Actions performed after the progress received
         *
         * @param values
         */

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }


}