/*
 * Copyright (C) 2016 Mykyta Shulhin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package stt.id12095939.com.slidertabstest.controller;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import java.io.File;
import java.text.ParseException;

import stt.id12095939.com.slidertabstest.model.MyDatabaseHelper;
import stt.id12095939.com.slidertabstest.presenter.PagerAdapter;
import stt.id12095939.com.slidertabstest.R;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;

public class MainActivity extends AppCompatActivity {

    private TabLayout mTabLayout;
    private MyDatabaseHelper mDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        if (doesDatabaseExist(getApplicationContext(), "psa11.sql")) {
            if (getIntent().getExtras() != null) {
                setContentView(R.layout.activity_main);
                Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
                setSupportActionBar(toolbar);
                getSupportActionBar().setDisplayShowTitleEnabled(false);
                setStatusColor();
                getWindow().setNavigationBarColor(Color.parseColor("#0097A7"));
                mTabLayout = (TabLayout) findViewById(R.id.tab_layout);
                mTabLayout.addTab(mTabLayout.newTab().setText("Frequent"));
                mTabLayout.addTab(mTabLayout.newTab().setText("Insights"));
                mTabLayout.addTab(mTabLayout.newTab().setText("Settings"));
                mTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
                final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
                final PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager(), mTabLayout.getTabCount());
                viewPager.setAdapter(adapter);
                viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
                mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        viewPager.setCurrentItem(tab.getPosition());
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {

                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {

                    }
                });

                if (getIntent().getExtras().getString("notification") == null) {
                    showCase(this, mTabLayout, "This is your navigation bar. Swipe around to see other tabs");
                } else {
                    Snackbar snackbar = Snackbar
                            .make(mTabLayout, "New transactions received.", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }

            } else {
                Intent login = new Intent(this, LoginActivity.class);
                startActivity(login);
            }
        } else {
            mDb = new MyDatabaseHelper(getApplicationContext());
            try {
                mDb.populateDb(5, 10);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Intent login = new Intent(this, LoginActivity.class);
            startActivity(login);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    private static boolean doesDatabaseExist(Context context, String dbName) {
        File dbFile = context.getDatabasePath(dbName);
        return dbFile.exists();
    }

    public void setStatusColor() {
        Window window = this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryCyanDark));
    }

    public void showCase(Activity activity, View view, String description) {

        ShowcaseConfig config = new ShowcaseConfig();
        config.setDelay(500); // half second between each showcase view

        MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(activity);

        sequence.setConfig(config);

        sequence.addSequenceItem(view,
                description, "Got it");

        sequence.start();
    }


}