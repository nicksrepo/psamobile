/*
 * Copyright (C) 2016 Mykyta Shulhin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package stt.id12095939.com.slidertabstest.model;

import android.util.Log;

import java.text.ParseException;
import java.util.LinkedList;

public class Brain {

    private LinkedList<Transaction> mJanuary = new LinkedList<>();
    private LinkedList<Transaction> mFabruary = new LinkedList<>();
    private LinkedList<Transaction> mMarch = new LinkedList<>();
    private LinkedList<Transaction> mApril = new LinkedList<>();
    private LinkedList<Transaction> mMay = new LinkedList<>();
    private LinkedList<Transaction> mJune = new LinkedList<>();
    private LinkedList<Transaction> mJuly = new LinkedList<>();
    private LinkedList<Transaction> mAugust = new LinkedList<>();
    private LinkedList<Transaction> mSeptember = new LinkedList<>();
    private LinkedList<Transaction> mOctober = new LinkedList<>();
    private LinkedList<Transaction> mNovember = new LinkedList<>();
    private LinkedList<Transaction> mDecember = new LinkedList<>();

    private int mInterval = 1;
    private LinkedList<LinkedList<Transaction>> mContainer = new LinkedList<>();
    private LinkedList<Transaction> mResult = new LinkedList<>();

    public Brain() {
        setUpContainer();
    }


    /**
     * This method sets up a month container list by adding month LinkedList objects.
     */

    public void setUpContainer() {
        mContainer.add(mJanuary);
        mContainer.add(mFabruary);
        mContainer.add(mMarch);
        mContainer.add(mApril);
        mContainer.add(mMay);
        mContainer.add(mJune);
        mContainer.add(mJuly);
        mContainer.add(mAugust);
        mContainer.add(mSeptember);
        mContainer.add(mOctober);
        mContainer.add(mNovember);
        mContainer.add(mDecember);
    }

    /**
     * This method sorts transaction according to month value.
     */
    public void sortTransactions(LinkedList<Transaction> transactions) {
        cleanUpContainer();
        for (Transaction transaction : transactions) {
            switch (transaction.getDate().substring(0, 2)) {
                case ("01"):
                    mContainer.get(0).add(transaction);
                    break;
                case ("02"):
                    mContainer.get(1).add(transaction);
                    break;
                case ("03"):
                    mContainer.get(2).add(transaction);
                    break;
                case ("04"):
                    mContainer.get(3).add(transaction);
                    break;
                case ("05"):
                    mContainer.get(4).add(transaction);
                    break;
                case ("06"):
                    mContainer.get(5).add(transaction);
                    break;
                case ("07"):
                    mContainer.get(6).add(transaction);
                    break;
                case ("08"):
                    mContainer.get(7).add(transaction);
                    break;
                case ("09"):
                    mContainer.get(8).add(transaction);
                    break;
                case ("10"):
                    mContainer.get(9).add(transaction);
                    break;
                case ("11"):
                    mContainer.get(10).add(transaction);
                    break;
                case ("12"):
                    mContainer.get(11).add(transaction);
                    break;
            }
        }
        showSortingResults();

    }

    /**
     * This method cleans up container data.
     */
    public void cleanUpContainer() {
        for (LinkedList current : mContainer) {
            current.clear();
        }
    }

    /**
     * This method is used for debugging purposes and shows analyzed transactions.
     */
    public void showSortingResults() {
        Log.d("BRAIN", "BRAIN ANALYZING...");
        for (LinkedList current : mContainer) {
            Log.d("BRAIN", "" + current.size());
        }
    }


    /**
     * This method analyzes transactions and defines which are repetitive.
     */
    public LinkedList<Transaction> analyzeTransactions(LinkedList<Transaction> transactions) throws ParseException {
        sortTransactions(transactions);
        mResult.clear();
        //Each month
        for (int month = 0; month < mContainer.size(); month++) {
            //Each transaction in month
            for (Transaction transaction : mContainer.get(month)) {
                //Comparing with next month transaction
                for (Transaction transaction_next : mContainer.get(month + mInterval)) {
                    //Check if index is in range
                    if (mContainer.size() >= month + mInterval) {
                        //Define frequent payment using
                        if (Integer.parseInt(transaction.getDate().substring(0, 2)) + mInterval == Integer.parseInt(transaction_next.getDate().substring(0, 2)) && transaction.getReceiver().equals(transaction_next.getReceiver()) && transaction.getAmount() == transaction_next.getAmount()) {
                            if (normalizationFilter(transaction)) {
                                Log.d("BRAIN", "Normalization filter launched on transaction: " + transaction.getReceiver() + " | " + transaction.getDate());
                            } else {
                                mResult.add(transaction_next);
                                Log.d("BRAIN", "Transaction " + transaction_next.getReceiver() + " | " + transaction_next.getDate() + " is marked as frequent with interval " + mInterval + " month.");
                            }

                        }
                    }
                }
            }
        }
        return mResult;
    }

    /**
     * This method deletes duplicated repetitive transactions.
     */
    public boolean normalizationFilter(Transaction transaction) {
        boolean exists = false;
        for (Transaction current : mResult) {
            if (current.getID() == transaction.getID()) {
                exists = true;
            }
        }
        return exists;
    }

    /**
     * This method calculates total payments cost.
     */
    public double getTotalPaymentCost(LinkedList<Transaction> target) {
        double total = 0;
        for (Transaction transaction : target) {
            total += transaction.getAmount();
        }
        return total;
    }

    /**
     * This method returns the most expensive Transaction object.
     */
    public Transaction getMostExpensive() {
        Transaction mostExpensive = null;
        double previous = 0;
        for (Transaction transaction : mResult) {
            if(!transaction.getImportantStatus()) {
                if (transaction.getAmount() > previous) {
                    mostExpensive = transaction;
                }
                previous = transaction.getAmount();
            }
        }
        return mostExpensive;
    }

    public LinkedList<Transaction> getResult() {
        return mResult;
    }

    /**
     * This method is used to search for transaction in the list according to receiver name value.
     */
    public Transaction findTransaction(String receiver){
        Transaction result = null;
        Log.d("BRAIN_SEARCH","SEARCH_QUERY:"+receiver);
        for(Transaction transaction: mResult){
            Log.d("BRAIN_SEARCH","NOW_ON:"+transaction.getReceiver());
            if(transaction.getReceiver().toUpperCase().equals(receiver.toUpperCase())){
                result  = transaction;
            }
        }
        return result;
    }

    /**
     * This method returns a list of payments and their status.
     */
    public LinkedList<String> getPaymentNames(){
        LinkedList<String> payments = new LinkedList<>();
        for(Transaction transaction: mResult){
            payments.add(transaction.getReceiver() + ", saved: " + transaction.getImportantStatus());
        }
        return payments;
    }

    /**
     * This method is used to search for transaction in the list according to ID value.
     */
    public Transaction findTransactionByID(String ID){
        Transaction result = null;
        for(Transaction transaction:mResult){
            if(String.valueOf(transaction.getID()).equals(ID)){
                result = transaction;
            }
        }
        return result;
    }
}
