/*
 * Copyright (C) 2016 Mykyta Shulhin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package stt.id12095939.com.slidertabstest.view;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.yalantis.phoenix.PullToRefreshView;

import java.text.ParseException;
import java.util.ArrayList;

import stt.id12095939.com.slidertabstest.model.Brain;
import stt.id12095939.com.slidertabstest.model.MyDatabaseHelper;
import stt.id12095939.com.slidertabstest.R;
import stt.id12095939.com.slidertabstest.model.Values;
import stt.id12095939.com.slidertabstest.controller.AssistantActivity;

public class TabFragment2 extends Fragment {
    private static View mTab;
    public static TextView sSavingGoal;
    private static MyDatabaseHelper mDb;
    private static Brain mBrain;
    private static Brain mSavingBrain;
    private static BarChart mChart;
    private Button mAdvice;
    private PullToRefreshView mPullToRefreshView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d("ACT", "ONCREATE");
        mTab = inflater.inflate(R.layout.tab_fragment_2, container, false);
        initialize();

        try {
            setUpChartTest();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        setSavingGoal();
        setUpSwipeRefresh();
        return mTab;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("ACT", "ONCRESUME");
    }


    private void setUpSwipeRefresh(){
        mPullToRefreshView = (PullToRefreshView) mTab.findViewById(R.id.pull_to_refresh_chart);
        mPullToRefreshView.setOnRefreshListener(new PullToRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPullToRefreshView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            setUpChartTest();
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        mPullToRefreshView.setRefreshing(false);
                    }
                }, 0);
            }
        });
    }


    private static ArrayList<IBarDataSet> getDataSet() throws ParseException {
        ArrayList<IBarDataSet> dataSets = null;

        ArrayList<BarEntry> valueSet1 = new ArrayList<>();
        BarEntry v1e1 = new BarEntry(Float.parseFloat(mDb.getSavingGoal()), 0); // Saving goal
        valueSet1.add(v1e1);

        BarDataSet barDataSet1 = new BarDataSet(valueSet1, "Saving goal");
        barDataSet1.setColor(Color.parseColor("#66B266"));

        ArrayList<BarEntry> valueSet2 = new ArrayList<>();
        double valueWithoutSaved = mBrain.getTotalPaymentCost(mBrain.analyzeTransactions(mDb.getTransactionObjects("all"))) - mSavingBrain.getTotalPaymentCost(mSavingBrain.analyzeTransactions(mDb.getTransactionObjects("saved")));
        BarEntry v2e1 = new BarEntry((float) valueWithoutSaved, 1); // Repetative payments
        valueSet2.add(v2e1);

        BarDataSet barDataSet2 = new BarDataSet(valueSet2, "Frequent payments");
        barDataSet2.setColor(Color.parseColor("#FF4C4C"));

        ArrayList<BarEntry> valueSet3 = new ArrayList<>();
        BarEntry v3e1 = new BarEntry((float) mBrain.getTotalPaymentCost(mBrain.analyzeTransactions(mDb.getTransactionObjects("saved"))), 2); // Repetative payments
        valueSet3.add(v3e1);

        BarDataSet barDataSet3 = new BarDataSet(valueSet3, "Amount saved");
        barDataSet3.setColor(Color.parseColor("#3299FF"));


        dataSets = new ArrayList<>();
        dataSets.add(barDataSet1);
        dataSets.add(barDataSet2);
        dataSets.add(barDataSet3);
        return dataSets;
    }

    private static ArrayList<String> getXAxisValues() {
        ArrayList<String> xAxis = new ArrayList<>();
        xAxis.add("Saving goal");
        xAxis.add("Payments");
        xAxis.add("Saved");
        return xAxis;
    }



    public static void setUpChartTest() throws ParseException {
        mChart = (BarChart) mTab.findViewById(R.id.chart);
        BarData data = new BarData(getXAxisValues(), getDataSet());
        mChart.setData(data);
        mChart.setDescription("Saving diagram");
        mChart.animateXY(1000, 1000);
        mChart.invalidate();
        mChart.animateY(3000);
    }



    public void setSavingGoal(){
        sSavingGoal.setText(Values.DOLLAR + mDb.getSavingGoal());
    }

    public void initialize() {
        mAdvice = (Button) mTab.findViewById(R.id.advice_button);
        mAdvice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent assistantLauncher =  new Intent(getContext(), AssistantActivity.class);
                startActivity(assistantLauncher);
            }
        });
        sSavingGoal = (TextView) mTab.findViewById(R.id.saving_text_view);
        mDb = new MyDatabaseHelper(getContext());
        mBrain = new Brain();
        mSavingBrain = new Brain();
        try {
            mBrain.analyzeTransactions(mDb.getTransactionObjects("all"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

}