/*
 * Copyright (C) 2016 Mykyta Shulhin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package stt.id12095939.com.slidertabstest.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;

public class MyDatabaseHelper extends SQLiteOpenHelper {

    ContentValues values;

    private static final int DATABASE_VERSION = 2;
    private static final String TRANSACTIONS_TABLE_NAME = "Transactions";
    private static final String LOGIN_TABLE_NAME = "Login";
    private static final String DATABASE_NAME = "psa11.sql";

    private static final String DICTIONARY_TABLE_CREATE =
            "CREATE TABLE " + TRANSACTIONS_TABLE_NAME + " (" +
                    "receiver" + " TEXT, " + "amount" + " TEXT, " + "date" + " TEXT, " +
                    "description" + " TEXT, " +
                    "id" + " TEXT, " + "important" + " TEXT ); ";

    private static final String LOGIN_TABLE_CREATE =
            "CREATE TABLE " + LOGIN_TABLE_NAME + " (" +
                    "client_number" + " TEXT, " + "password" + " TEXT, " + "full_name" + " TEXT, " + "saving_goal" + " TEXT ); ";


    public MyDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        values = new ContentValues();
    }

    public void createLoginTable(SQLiteDatabase db) {
        db.execSQL(LOGIN_TABLE_CREATE);
        ContentValues insertValues = new ContentValues();
        insertValues.put("client_number", "999999");
        insertValues.put("password", "password");
        insertValues.put("full_name", "Nickita Shulhin");
        insertValues.put("saving_goal", "0");
        db.insert(LOGIN_TABLE_NAME, null, insertValues);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DICTIONARY_TABLE_CREATE);
        createLoginTable(db);
        Log.d("DB", "Database has been created.");
    }

    public void populateDb(int months, int transactionsPerMonth) throws ParseException {
        String receiver;
        String amount;
        String id;
        for (int i = 1; i < months + 1; i++) {
            for (int t = 0; t < transactionsPerMonth; t++) {
                SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
                String date = format.format(new Date());
                SQLiteDatabase db = this.getWritableDatabase();
                ContentValues insertValues = new ContentValues();
                if (t <= 3) {
                    //receiver = Values.merchants[RandomGen.getRandom(Values.merchants.length, 0)];
                    receiver = Values.merchants[t];
                    amount = Values.amounts[t];
                    id = Values.transaction_id[t];
                } else {
                    receiver = RandomGen.getRandom(999999999, 100000000) + "";
                    amount = RandomGen.getRandom(19, 1) + Values.DOT + RandomGen.getRandom(99, 10);
                    id = RandomGen.getRandom(999999, 100000)+"";
                }
                insertValues.put("receiver", receiver);
                insertValues.put("amount", amount);
                insertValues.put("date", "0" + i + "/21/2016");
                insertValues.put("description", Values.descriptions[RandomGen.getRandom(Values.descriptions.length, 0)]);
                insertValues.put("id", id);
                insertValues.put("important", Values.important[1]);
                db.insert("Transactions", null, insertValues);
                Log.d("DB", "DATE_FORMAT_IS: " + date);
            }
        }
    }

    public void deleteAllFromTable(String tableName) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(tableName, null, null);
    }

    public LinkedList<Transaction> getTransactionObjects(String status) {
        String selectQuery = "";
        if(status.equals("saved")){
            selectQuery = "SELECT  * FROM " + TRANSACTIONS_TABLE_NAME + " WHERE important = 'true'";
        }
        if(status.equals("all")){
            selectQuery = "SELECT  * FROM " + TRANSACTIONS_TABLE_NAME;
        }

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        LinkedList<Transaction> data = new LinkedList<>();

        if (cursor.moveToFirst()) {
            do {
                data.add(new Transaction(cursor.getString(cursor.getColumnIndex("receiver")), Double.parseDouble(cursor.getString(cursor.getColumnIndex("amount"))), cursor.getString(cursor.getColumnIndex("date")), cursor.getString(cursor.getColumnIndex("description")), Integer.parseInt(cursor.getString(cursor.getColumnIndex("id"))), Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex("important")))));
            } while (cursor.moveToNext());
        }
        cursor.close();

        return data;
    }

    public Client getClientObject() {
        String selectQuery = "SELECT  * FROM " + LOGIN_TABLE_NAME;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        Client client = null;

        if (cursor.moveToFirst()) {
            do {
                client = new Client(Integer.parseInt(cursor.getString(cursor.getColumnIndex("client_number"))), cursor.getString(cursor.getColumnIndex("password")), cursor.getString(cursor.getColumnIndex("full_name")), Integer.parseInt(cursor.getString(cursor.getColumnIndex("saving_goal"))));

            } while (cursor.moveToNext());
        }
        cursor.close();

        return client;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public static String getTransactionsTableName() {
        return TRANSACTIONS_TABLE_NAME;
    }

    public static String getLoginTableName() {
        return LOGIN_TABLE_NAME;
    }

    public String getSavingGoal() {

        String selectQuery = "SELECT  saving_goal FROM " + LOGIN_TABLE_NAME;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        String savingGoal = "";
        if (cursor.moveToFirst()) {
            do {
                savingGoal = cursor.getString(cursor.getColumnIndex("saving_goal"));
            } while (cursor.moveToNext());
        }
        cursor.close();

        return savingGoal;
    }

    public void updateSavingGoal(String goal) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("saving_goal", goal);
        db.update(LOGIN_TABLE_NAME, cv, "client_number=" + getClientObject().getClientNumber(), null);
    }

    public void updateTransactionStatus(String status, String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("important", status);
        db.update(TRANSACTIONS_TABLE_NAME, cv, "id=" + id, null);
        Log.d("DB", "UPDATED ID: "+id+" WITH NEW VALUE: "+status);
    }
}
