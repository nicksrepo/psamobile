/*
 * Copyright (C) 2016 Mykyta Shulhin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package stt.id12095939.com.slidertabstest.presenter;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import stt.id12095939.com.slidertabstest.model.Message;
import stt.id12095939.com.slidertabstest.R;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MyViewHolder> {

    private List<Message> mMessageList;
    public View mView;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView content, avatar;
        public LinearLayout messageLayout;


        public MyViewHolder(View view) {
            super(view);
            mView = view;
            avatar = (TextView) view.findViewById(R.id.alice_avatar);
            content = (TextView) view.findViewById(R.id.message_content);
            messageLayout = (LinearLayout) view.findViewById(R.id.message_wrapper);
        }
    }


    public MessageAdapter(List<Message> messageList) {
        this.mMessageList = messageList;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.message_layout, parent, false);
        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Message message = mMessageList.get(position);
        if (message.getSender().equals("System")) {
            holder.avatar.setVisibility(View.VISIBLE);
            holder.content.setBackgroundResource(R.drawable.message_bubble);
            holder.content.setTextColor(Color.parseColor("#000000"));
            holder.messageLayout.setGravity(Gravity.START);
        }
        else{
            holder.avatar.setVisibility(View.GONE);
            holder.content.setBackgroundResource(R.drawable.message_bubble_out);
            holder.content.setTextColor(Color.parseColor("#FFFFFF"));
            holder.messageLayout.setGravity(Gravity.END);
        }
        holder.content.setText(message.getContent());
    }


    @Override
    public int getItemCount() {
        return mMessageList.size();
    }

}