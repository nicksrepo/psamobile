/*
 * Copyright (C) 2016 Mykyta Shulhin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package stt.id12095939.com.slidertabstest.controller;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import java.text.ParseException;
import java.util.ArrayList;

import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;
import stt.id12095939.com.slidertabstest.model.Alice;
import stt.id12095939.com.slidertabstest.model.Message;
import stt.id12095939.com.slidertabstest.presenter.MessageAdapter;
import stt.id12095939.com.slidertabstest.R;
import stt.id12095939.com.slidertabstest.model.Transaction;

public class AssistantActivity extends AppCompatActivity {
    private ArrayList<Message> mMessageList = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private MessageAdapter mAdapter;
    private EditText mInput;
    private Button mSendMessage;
    private Alice mAlice;
    private SmoothProgressBar mProgressBar;
    private ImageView mAssistantBack;
    private Transaction mAskedTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assistant);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getWindow().setNavigationBarColor(Color.parseColor("#0097A7"));
        mRecyclerView = (RecyclerView) findViewById(R.id.assistant_recycler_view);
        mAdapter = new MessageAdapter(mMessageList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);
        try {
            initialize();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(getIntent().getExtras()!=null){
            AliceAsync aliceJob = new AliceAsync();
            aliceJob.execute("Advice for ID: "+getIntent().getExtras().getString("ID"));
        }

    }

    /**
     * This method initializes class attributes.
     */
    public void initialize() throws ParseException {
        mInput = (EditText) findViewById(R.id.message_input);
        mSendMessage = (Button) findViewById(R.id.send_message_button);
        mAlice = new Alice(getApplicationContext());
        mMessageList.add(new Message("System", "User", mAlice.greetings(), "text"));
        mAdapter.notifyItemInserted(mAdapter.getItemCount());
        mProgressBar = (SmoothProgressBar) findViewById(R.id.smooth_progress_bar_assistant);
        mAssistantBack = (ImageView) findViewById(R.id.assistant_back_button);
        mInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRecyclerView.smoothScrollToPosition(mAdapter.getItemCount() - 1);
            }
        });
        mAssistantBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mInput.getText().toString().isEmpty()||mInput.getText().toString().equals("")) {
                    mInput.setError("Please, type your message here");
                }else{
                    mMessageList.add(new Message("User", "System", mInput.getText().toString(), "text"));
                    AliceAsync aliceJob = new AliceAsync();
                    aliceJob.execute(mInput.getText().toString());
                }


            }
        });
    }

    /**
     * This class represent an AsyncTask loader for Alice operation execution.
     */
    public class AliceAsync extends AsyncTask<String, Void, Integer> {


        /**
         * Actiones that must be performed before the execution
         */

        @Override
        protected void onPreExecute() {
            mProgressBar.setVisibility(View.VISIBLE);
        }


        /**
         * Actiones that must be performed in a background
         *
         * @param
         * @return
         */

        @Override
        protected Integer doInBackground(String... values) {
            int result = 0;
            try {
                Thread.sleep(1000);
                mMessageList.add(new Message("System", "User", mAlice.ask(values[0]), "text"));
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return result;
        }

        /**
         * Actions that must be performed after execution
         *
         * @param integer
         */

        @Override
        protected void onPostExecute(Integer integer) {
            mProgressBar.setVisibility(View.GONE);
            mAdapter.notifyItemInserted(mAdapter.getItemCount());
            mRecyclerView.smoothScrollToPosition(mAdapter.getItemCount() - 1);
            mInput.setText("");
        }

        /**
         * Actiones performed after the progress received
         *
         * @param values
         */

        @Override
        protected void onProgressUpdate(Void... values) {


        }
    }

}
