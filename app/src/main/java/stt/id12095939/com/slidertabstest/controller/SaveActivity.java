/*
 * Copyright (C) 2016 Mykyta Shulhin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package stt.id12095939.com.slidertabstest.controller;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.roughike.swipeselector.SwipeItem;
import com.roughike.swipeselector.SwipeSelector;
import com.truizlop.fabreveallayout.FABRevealLayout;

import java.text.DecimalFormat;
import java.util.ArrayList;

import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;
import stt.id12095939.com.slidertabstest.model.MyDatabaseHelper;
import stt.id12095939.com.slidertabstest.R;
import stt.id12095939.com.slidertabstest.model.Values;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;

public class SaveActivity extends AppCompatActivity {


    private double mIntentAmount;
    private String mIntentReceiver;
    private String mIntentDate;
    private MyDatabaseHelper mDb;
    private int mSavingGoal;
    private String mId;
    private String mImportantStatus;
    private Button mButtonSave;
    private Button mButtonCancel;
    private Button mButtonAskAdvisor;
    private BarChart mBarChart;
    private FABRevealLayout mFabRevealLayout;
    private TextView mSaveDialog;
    private SmoothProgressBar mProgressBar;
    private View mView;
    private String mDescription;
    private FloatingActionButton mFab;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save);
        getWindow().setNavigationBarColor(Color.parseColor("#0097A7"));
        initialize();
        implementIntentData();
        swipeSelectorSetup();
        setUpButtons();
        setUpChartTest();
    }

    public void initialize() {
        mDb = new MyDatabaseHelper(getApplicationContext());
        mSavingGoal = Integer.parseInt(mDb.getSavingGoal());
        mBarChart = (BarChart) findViewById(R.id.save_chart);
        mButtonSave = (Button) findViewById(R.id.button_save_dialog);
        mButtonCancel = (Button) findViewById(R.id.button_cancel_dialog);
        mFabRevealLayout = (FABRevealLayout) findViewById(R.id.fab_reveal_layout);
        mSaveDialog = (TextView) findViewById(R.id.dialog_save);
        mProgressBar = (SmoothProgressBar) findViewById(R.id.smooth_progress_bar);
        mButtonAskAdvisor = (Button) findViewById(R.id.but_ask_assistant);
        mFab = (FloatingActionButton) findViewById(R.id.fab);
    }

    public void setUpButtons() {
        mButtonAskAdvisor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent launcher = new Intent(getApplicationContext(),AssistantActivity.class);
                launcher.putExtra("ID",mId);
                startActivity(launcher);
            }
        });
        mButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFabRevealLayout.revealMainView();
            }
        });

        if (mImportantStatus.equals("true")) {
            mSaveDialog.setText("You already saved this payment. Unsave?");
            mButtonSave.setText("Unsave");
            mButtonAskAdvisor.setEnabled(false);
            showCaseNoAssistant();

            mButtonAskAdvisor.setTextColor(Color.RED);
            mButtonSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mView = v;
                   LoginAsync launch = new LoginAsync();
                    launch.execute("false");
                }
            });
        } else {
            showCase();
            mButtonSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mView = v;
                    LoginAsync launch = new LoginAsync();
                    launch.execute("true");
                }
            });
        }
    }


    public void swipeSelectorSetup() {
        SwipeSelector swipeSelector = (SwipeSelector) findViewById(R.id.swipeSelector);
        swipeSelector.setItems(
                // The first argument is the value for that item, and should in most cases be unique for the
                // current SwipeSelector, just as you would assign values to radio buttons.
                // You can use the value later on to check what the selected item was.
                // The value can be any Object, here we're using ints.
                new SwipeItem(0, mIntentReceiver, mDescription),
                new SwipeItem(1, Values.DOLLAR + String.valueOf(mIntentAmount), "Amount spent per month."),
                new SwipeItem(2, Values.DOLLAR + String.valueOf(doubleFormatter(mIntentAmount * 12)), "Amount spent per year."),
                new SwipeItem(3, setCalculationResult(), " of your saving goal per month.")
        );
    }

    public void implementIntentData() {
        mIntentAmount = getIntent().getExtras().getDouble("amount");
        mIntentReceiver = getIntent().getExtras().getString("receiver");
        mIntentDate = getIntent().getExtras().getString("date");
        mId = String.valueOf(getIntent().getExtras().getInt("ID"));
        mImportantStatus = getIntent().getExtras().getString("status");
        mDescription = getIntent().getExtras().getString("description");

    }


    private ArrayList<IBarDataSet> getDataSet() {
        ArrayList<IBarDataSet> dataSets = null;

        ArrayList<BarEntry> valueSet1 = new ArrayList<>();
        BarEntry v1e1 = new BarEntry(Float.parseFloat(String.valueOf(mIntentAmount)), 1);
        valueSet1.add(v1e1);

        BarDataSet barDataSet1 = new BarDataSet(valueSet1, mIntentReceiver);
        barDataSet1.setColor(Color.parseColor("#FF4C4C"));

        ArrayList<BarEntry> valueSet2 = new ArrayList<>();
        BarEntry v2e1 = new BarEntry(mSavingGoal, 0);
        valueSet2.add(v2e1);

        BarDataSet barDataSet2 = new BarDataSet(valueSet2, "Saving goal");
        barDataSet2.setColor(Color.parseColor("#66B266"));

        dataSets = new ArrayList<>();
        dataSets.add(barDataSet1);
        dataSets.add(barDataSet2);

        return dataSets;
    }

    private static ArrayList<String> getXAxisValues() {
        ArrayList<String> xAxis = new ArrayList<>();
        xAxis.add("Saving goal");
        xAxis.add("Per month");
        return xAxis;
    }



    public void setUpChartTest() {
        BarData data = new BarData(getXAxisValues(), getDataSet());
        mBarChart.setData(data);
        mBarChart.setDescription("Saving diagram");
        mBarChart.animateXY(1000, 1000);
        mBarChart.invalidate();
        mBarChart.animateY(3000);
    }



    public String setCalculationResult() {
        checkValuesDEBUG();
        String result = calculateSavings(mSavingGoal, mIntentAmount);
        String output = Values.DOLLAR + doubleFormatter(mIntentAmount) + " equals to " + result + Values.PERCENT;
        return output;
    }

    public void checkValuesDEBUG() {
        Log.d("NULL_BUG", "INTAMOUNT:" + mIntentAmount + " INTRECEIVER:" + mIntentReceiver + " INTDATE:" + mIntentDate + " DBSAVINGGOAL:" + mSavingGoal);
    }

    public String calculateSavings(int goal, double amount) {
        return doubleFormatter((amount * 100) / goal);
    }


    public String doubleFormatter(double number) {
        DecimalFormat df = new DecimalFormat("#0.00");
        return df.format(number);
    }


    public class LoginAsync extends AsyncTask<String, Void, Integer> {


        /**
         * Actiones that must be performed before the execution
         */

        @Override
        protected void onPreExecute() {
            mProgressBar.setVisibility(View.VISIBLE);
        }


        /**
         * Actiones that must be performed in a background
         *
         * @param
         * @return
         */

        @Override
        protected Integer doInBackground(String... values) {

            try {
                Thread.sleep(2000);
                mDb.updateTransactionStatus(values[0], mId);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        /**
         * Actions that must be performed after execution
         *
         * @param integer
         */

        @Override
        protected void onPostExecute(Integer integer) {
            mProgressBar.setVisibility(View.GONE);
            Snackbar.make(mView, R.string.success_text, Snackbar.LENGTH_LONG).show();
            mFabRevealLayout.revealMainView();
        }

        /**
         * Actiones performed after the progress received
         *
         * @param values
         */

        @Override
        protected void onProgressUpdate(Void... values) {


        }
    }

    public void showCaseNoAssistant() {

        ShowcaseConfig config = new ShowcaseConfig();
        config.setDelay(500); // half second between each showcase view

        MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(this);

        sequence.setConfig(config);

        sequence.addSequenceItem(mButtonAskAdvisor,
                "You can't ask advice from assistant when payment is marked as 'Saved'. You have to unsave the payment first to use advice.", "Got it");
        sequence.start();
    }

    public void showCase() {

        ShowcaseConfig config = new ShowcaseConfig();
        config.setDelay(500); // half second between each showcase view

        MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(this);

        sequence.setConfig(config);

        sequence.addSequenceItem(mFab,
                "Press this button to reveal an action menu where you can mark your payments.", "Got it");
        sequence.addSequenceItem(mButtonAskAdvisor,
                "You can always ask for advise your personal assistant - Alice", "Got it");
        sequence.start();
    }

}
